-   [1. Repo overview](#1-repo-overview)
-   [2. Development setup](#2-development-setup)
-   [3. Development workflow](#3-development-workflow)
-   [4. Shared components](#4-shared-components)
-   [Linting and styling](#linting-and-styling)
-   [Git hooks](#git-hooks)
-   [Installing packages](#installing-packages)
-   [CICD](#cicd)
-   [Best practice](#best-practice)
-   [TODO](#todo)
-   [Testing](#testing)

## 1. Repo overview

A frontend repo containing multiple projects and common code that can be shared between the projects.
Having multiple projects allow us to seperate different HMIs from each other reducing unescesary coupling. 
Example of projects (sometimes in our doc refered to apps)
- wec
- farm
- seal rig
- mobile app
- admin/user managment

### Managing dependencies and pnpm workspaces
Each project and the shared components contain dependencies. These are managed by pnpm workspaces.
Dependencies are specified in the package.json file either in each project, in the shared folder or in the root (where global dependencies are specified)

NOTE: today shared components are consumed by importing them as files which makes most benefits listed below invalid (ie they will use the dependencies from the consumer and not the dependencies specified in shared).

Having seperate package.json files for each project allows for flexible managing of version. 
For example 
- we can update vue in project A without updateing it in project B.
- we can update vue in project A to a new version while the shared components remain on older version.


Additional benefits of pnpm workspaces are
- easily share global lint rules for all projects and shared code
- quicker installation and setup, each package is only installed once (i.e one node_modules fodler for whole repo)

Without pnpm workspaces 
- shared code would use the dependencies of the consumer of the shared code (which might not be compatible) 
- with pnpm workspaces its possible to hot reload when changes occur in shared code.
- share code using a package registry might be a solution but it creates a lot of overhead releasing new packages. 


## 2. Development setup
install dependencies in package.json across the whole project `pnpm install` (from root folder)

Install the recommended extensions for VS code, see the file `.vscode/extensions.json`

You can run `pnpm install-completion` to set up autocomplete functionality for pnpm commands when pressing tab.

## 3. Development workflow
1. Start vscode with wsl
2. Build from the dockerfile `docker build . --tag node:0.1.0` (only needed when Dockerfile has changed)
3. Run the image `./runWithDocker.py -np -v`
4. In vscode ctrl + shift + p search attach to running container and then select the running container.
5. Ensure the workspace in .vscode is used.
6. Run `pnpm install` (from root folder) (only first time)


Compared to npm "run" is not needed when running the scripts in packages.json

`pnpm --filter "project-name"` can be used in combination with most pnpm commands to only target a specific project

Examples:
Starting a dev server for wec app from root run `pnpm --filter wec dev`
Starting a dev server for wec app from the wec folder `pnpm dev`

Start chrome browser in your windows and point it to localhost:5173


## 4. Shared components

Simply a root level folder that is referenced by alias. In pnpm its handled as a separate package which place it upon the user to ensure that all dependencies that are used in shared and in each project are at the same compatible version. Because when referensing the shared component one will use the dependency versions in the project folder, not the from the shared folder.

## Linting and styling

Lint rules are set on a global level in the root folder. If one needs to make custom rules for individual projects add a new config file (eg. .styleling.json or .eslintrc.js) in the project folder instead. This will then override the default rules.
Changing these configs might require to restart vscode.

-   eslint

    Code-quality rules for javascript and vue is done using eslint

-   stylelint

    Code-quality rules for css and scss is done with stylelint. When you install it can be good to disable vs codes default rules for css. This should be done automatically in the settings.json file.

-   prettier

    Formatting rules (styling) is done using prettier. It runs as soon as the file are saved. Pay attention to for example when it breaks line adn ensure it looks nice.
    If we decide to not fix files on save but rather show a red line on isues it prettier needs to be installed as an eslint rule.

## Git hooks

These hooks are setup up using lefthook. The hooks can be override with git no-verify. But the pipeline will fail.

-   Pre-commit. A commit will fail if any of the below checks fail.
    -   eslint check
    -   stylint check
    -   prettier check
-   Pre-push (to do)

## Installing packages

Packages installed in root package.json will be global for all projects.
This is relevant for tools that are generic for all projects, eg prettier, linters etc with common rules across all projects.

`pnpm add -Dw stylelint-config-recommended-scss`

To install only in a specifc project one can run

`pnpm --filter shared add axios`

If you modify a dependency inside package.json remember to run install att root
`pnpm install`

## CICD

pnpm scripts are run from root level with the filter flag to decide which project they should run on.
They assume of course that the scirpt exist in each project. So the script in each package.json will most likely be duplactes but no way around this.

## Folder structure in a project
- src: "is the main folder, stores the source code"
- public: "stores static files like Favicons and images ..."
- src/assets: "While static assets like images might be stored in the public folder for direct access, assets that require processing (like SCSS files, SVGs that are used directly in components, or images that are imported into components for dynamic use) are typically kept in this folder."
- src/components: this folder holds components that are shared across multiple components and views (within the same project). If a component is shared between projects it should be moved to the /shared/componets/ folder.
- src/router/index.js: "This file structures the navigation of the page, in the url, but also the navigation bar/menu".
- src/stores: "The different stores holds centralized application state, both for meta things but also for smaller scope like components and such."
- src/views": "The different pages - containers"
- src/App.vue: "The origin of the application, sets up the overall UI"


## Best practice
Don't move things into /appA/src/components/ folder that aren't shared across multiple components and similar for the /shared folder where only tools that are shared between multiple projects are placed (or anticipated to be shared). Also don't move things to a higher level than they get used in general (such as moving a component-specific utility function into the src/utils level utils).





## TODO

-   Not working
-   pipeline in merge requests
-   Stylelint
-   gitlab runner
    -   Currently pipeline has been running on a local runner
    -   to start runner, dont run as sudo,
        `gitlab-runner list`
        `gitlab-runner run`

https://docs.gitlab.com/runner/commands/

## Testing

vitest.workspace.js specify the workspaces. Even if the tests show up in vscode test view they will not run if the project is not specified in this file.

For testing se apps/wec/test/readme

Structure and order in each test.

-   mockeddata
-   input data to test
-   expected results
-   run test
-   assert
