#!/usr/bin/env python3
import subprocess
import sys
import getpass
import argparse
import os
import itertools
from pythonUtils import run_cmd_and_return_output


DOCKER_IMAGE = "node:0.1.0"
DOCKER_HOSTNAME = "wec-c5-dev-container"


def create_docker_command(inputs_args, git_root, current_hostname, verbose):
    command = []
    if current_hostname == DOCKER_HOSTNAME:
        # If the script is running inside the container, just execute the command.
        if verbose:
            print(f"{os.path.basename(__file__)}: create_docker_command: Inside container, executing {inputs_args}")
        if inputs_args:
            command.extend(inputs_args)
        else:
            print(f"{os.path.basename(__file__)}: Inside container and no arguments provided")
            print(f"{os.path.basename(__file__)}: Nothing to do, exiting")
            sys.exit(-1)
    else:
        # The scripts is not running in the container,
        # start the container and mount all needed directories.
        command = ["docker", "container", "run", "--rm"]
        # Mount and set wordir dir to machine_controller
        command.extend(["-v", f"{git_root}:{git_root}", "--workdir", f"{git_root}"])
        # Capture User and Group ID, so the user can run as itself inside the containter
        UID = subprocess.run(["id", "-u"], stdout=subprocess.PIPE, universal_newlines=True).stdout.strip()
        GID = subprocess.run(["id", "-g"], stdout=subprocess.PIPE, universal_newlines=True).stdout.strip()
        USERNAME = getpass.getuser()
        # Add user and group identifers files for user autentication
        command.extend(["-v", "/etc/passwd:/etc/passwd:ro", "-v", "/etc/group:/etc/group:ro", "--user", f"{UID}:{GID}"])
        command.extend(["-v", f"/home/{USERNAME}:/home/{USERNAME}"])
        # Several unittest will try to write to /mnt/logs. Mount /tmp into containers /mnt,
        # so all files disappear when host is restarted.
        command.extend(["-v", "/tmp/:/mnt/"])
        # Insert provided docker image tag and arguments.
        if inputs_args:
            command.extend(["-i", "-a", "stdin", "-a", "stdout", "-a", "stderr", "--log-driver=none"])
            command.extend([DOCKER_IMAGE])
            command.extend(inputs_args)
        else:
            # If no input args, create an interactive shell and add docker image tag
            command.extend(["-it", "--hostname", DOCKER_HOSTNAME])
            command.extend([DOCKER_IMAGE])
    if verbose:
        print(f"{os.path.basename(__file__)}: create_docker_command: Created command {command}")
    return command


def create_docker_command_list(inputs_args, git_root, current_hostname, verbose):
    """
    Create docker commands_list from input args.

    Input:
        inputs_args: given argument to program
        git_root: absolut path to root of git repository
        current_host: Current host name of the environment
        verbose: print verbose output if true
    Return:
        command_list: List of command to execute
    """

    args_list = []
    if inputs_args and any("|" == arg for arg in inputs_args):
        # Split into several commands if piping is requested
        args_list = [list(group) for devider, group in itertools.groupby(inputs_args, lambda element: element == "|") if not devider]
    if verbose:
        print(f"{os.path.basename(__file__)}: create_docker_command_list: args_list {args_list}")
    command_list = []
    if not args_list:
        command_list.append(create_docker_command(inputs_args, git_root, current_hostname, verbose))
    else:
        for input in args_list:
            command_list.append(create_docker_command(input, git_root, current_hostname, verbose))
    if verbose:
        print(f"{os.path.basename(__file__)}: create_docker_command_list: command_list {command_list}")
    return command_list


def run_docker_image(inputs_args, git_root, current_hostname, capture_output, verbose):
    """
    Runs defined Docker image from global variable DOCKER_IMAGE
    Will mount several common folders to imge before running.
    The image will be run as the same user as the program is executed as.

    Input:
        inputs_args: given argument to program
        git_root: absolut path to root of git repository
        current_host: Current host name of the environment
        verbose: print verbose output if true
    """
    if verbose:
        print(f"{os.path.basename(__file__)}: run_docker_image: inputs_args {inputs_args}")
    command_list = create_docker_command_list(inputs_args, git_root, current_hostname, verbose)
    # Run command
    try:
        stdout = None
        stderr = None
        while command_list:
            cmd = command_list.pop(0)
            if verbose:
                print(f"{os.path.basename(__file__)}: run_docker_image: Commands left {len(command_list)}")
                print(f"{os.path.basename(__file__)}: run_docker_image: Full command {cmd}")
                split_here = cmd.index(DOCKER_IMAGE)
                print(f"{os.path.basename(__file__)}: run_docker_image: Running inside container {cmd[split_here+1:]}")

            if not inputs_args or (len(command_list) == 0 and not stdout):
                subprocess.run(cmd, check=True)
            else:
                if stdout:
                    if verbose:
                        decode_output = stdout.decode("utf-8").strip()
                        print(f"{os.path.basename(__file__)}: run_docker_image: Creating {cmd} with stdin:\n{decode_output}")
                    new_process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                    stdout, stderr = new_process.communicate(input=stdout)
                else:
                    stdout, stderr = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()
                if verbose:
                    if stdout:
                        decode_stdout = stdout.decode("utf-8")
                        print(f"{os.path.basename(__file__)}: run_docker_image: {cmd} stdout {decode_stdout}")
                    if stderr:
                        decode_stderr = stderr.decode("utf-8")
                        print(f"{os.path.basename(__file__)}: run_docker_image: {cmd} stderr {decode_stderr}")
        # No more cmd to process, printout result
        if inputs_args:
            if stdout:
                print(stdout.decode("utf-8").strip())
            if stderr:
                print(stderr.decode("utf-8").strip())
    except subprocess.CalledProcessError as e:
        if verbose or not capture_output:
            print(f"{os.path.basename(__file__)}: run_docker_image: Failed {args.cmd} image due to: {str(e)}")
        sys.exit(-1)
    except Exception as e:
        if verbose or not capture_output:
            print(f"{os.path.basename(__file__)}: run_docker_image: Failed running image due to: {str(e)}")
        sys.exit(-1)


def pull_image(verbose, capture_out):
    """
    Pull predefined image from DOCKER_IMAGE to local host.

    Input:
        verbose: print verbose output if true

    """
    try:
        if verbose:
            print(f"{os.path.basename(__file__)}: pull_image: Pulling {DOCKER_IMAGE}")
        subprocess.run(["docker", "image", "pull", DOCKER_IMAGE], check=True, capture_output=capture_out)
    except subprocess.CalledProcessError as e:
        print(f"{os.path.basename(__file__)}: pull_image: Failed pulling image due to:{str(e)}")
        sys.exit(-1)
    except Exception as e:
        print(f"{os.path.basename(__file__)}: pull_image: Unexpected exception:{str(e)}")
        sys.exit(-1)


def main(args):
    if args.verbose:
        print(f"{os.path.basename(__file__)}: input args: {args}")
    # Clean away 1:st arg since that is the name of the executing program.
    if len(args.cmd) == 1:
        # Compound command, split for subprocess to be able run it.
        args.cmd = args.cmd[0].replace(";", "&&").split("&&")
    if args.verbose:
        print(f"{os.path.basename(__file__)}: after splitting input args: {args}")

    # Get hostname of running enviroment
    hostname = run_cmd_and_return_output(["hostname"])
    if args.verbose:
        print(f"{os.path.basename(__file__)}: hostname: {hostname}")
    # Pull defind image from Gitlab
    if not args.nopull and hostname != DOCKER_HOSTNAME:
        pull_image(args.verbose, args.captureOutput)
    git_root = run_cmd_and_return_output(["git", "rev-parse", "--show-toplevel"])
    if args.verbose:
        print(f"{os.path.basename(__file__)}: git_root: {git_root}")
    # Allways run docker image once for interactive mode to work
    if not args.cmd:
        run_docker_image(args.cmd, git_root, hostname, args.captureOutput, args.verbose)
    else:
        for arg in args.cmd:
            # Run docker image with arguments
            run_docker_image(arg.split(), git_root, hostname, args.captureOutput, args.verbose)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog=f"{sys.argv[0]}",
        description="Mounts and runs predefined DOCKER_IMAGE from registry",
    )
    parser.add_argument("cmd", nargs="*", help="Define what commands should be executed inside container")
    parser.add_argument("-np", "--nopull", default=False, help="Set to prevent from pulling from Docker Registry", action="store_true")
    parser.add_argument("-co", "--captureOutput", default=False, help="Set to capture cmd output and redirect to stdout", action="store_true")
    parser.add_argument("-v", "--verbose", default=False, help="Print verbose output", action="store_true")
    args = parser.parse_args()
    main(args)
    if not args.captureOutput:
        print("Success!")
