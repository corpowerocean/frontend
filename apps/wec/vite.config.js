import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

// https://vitejs.dev/config/
export default defineConfig({
    server: {
        host: true, // Allow access from any host
        port: 5173, // Port to serve your Vite app
        watch: {
            usePolling: true,
        },
    },
    optimizeDeps: {
        include: ['@vue/runtime-core'], // If using Vue 3, include Vue runtime for HMR to work properly
    },
    plugins: [
        vue({
            template: { transformAssetUrls },
        }),
        // @quasar/plugin-vite options list:
        // https://github.com/quasarframework/quasar/blob/dev/vite-plugin/index.d.ts
        quasar({
            autoImportComponentCase: 'combined',
            sassVariables: 'src/quasar-variables.sass',
        }),
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src/', import.meta.url)),
            '@components': fileURLToPath(
                new URL('./src/components/', import.meta.url)
            ),
            '@shared': fileURLToPath(
                new URL('../../shared/src/', import.meta.url)
            ),
        },
    },
})
