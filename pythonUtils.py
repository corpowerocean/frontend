#!/usr/bin/env python3
import subprocess
import sys

def run_cmd_and_return_output(cmd):
    """
    Runs cmd and exit if failed.
    Return output of command
    """
    try:
        return_str = subprocess.run(cmd, stdout=subprocess.PIPE, universal_newlines=True).stdout.strip()
    except subprocess.CalledProcessError as e:
        print(f"{__name__}: {str(e)}")
        sys.exit(-1)
    except Exception as e:
        print(f"{__name__}: Unexpected exception :{str(e)}")
        sys.exit(-1)
    return return_str
