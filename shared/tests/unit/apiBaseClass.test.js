import { beforeEach, expect, test, describe, vi } from 'vitest'
import { ApiBaseClass } from '@shared/services/apiBaseClass'

import axios from 'axios'

vi.mock('axios')

const defaultAxiosConfig = {
    method: 'get',
    url: 'http://127.0.0.1:3001/',
    timeout: 30000,
    data: {},
}

describe('responses', () => {
    beforeEach(() => {
        axios.mockReset()
    })
    test('axios config - specific data', async () => {
        const axiosConfig = {
            method: 'post',
            url: defaultAxiosConfig['url'],
            timeout: defaultAxiosConfig['timeout'],
            data: { signal: 'test' },
        }

        const apiBaseClass = new ApiBaseClass()
        apiBaseClass.post({ signal: 'test' })

        expect(axios).toHaveBeenCalledWith(axiosConfig)
    })

    test('success - unspecified endpoint, no data object', async () => {
        const mockedResponseData = {
            data: {},
            status: 200,
        }
        axios.mockResolvedValue(mockedResponseData)

        const apiBaseClass = new ApiBaseClass()
        const response = await apiBaseClass.get()

        expect(axios).toHaveBeenCalledWith(defaultAxiosConfig)
        expect(response).toStrictEqual(mockedResponseData)
    })

    test('success - unspecified endpoint, empty data', async () => {
        const mockedResponseData = {
            data: {},
            status: 200,
        }
        axios.mockResolvedValue(mockedResponseData)

        const apiBaseClass = new ApiBaseClass()
        const response = await apiBaseClass.get()

        expect(axios).toHaveBeenCalledWith(defaultAxiosConfig)
        expect(response).toStrictEqual(mockedResponseData)
    })

    test('failure - wrong URL (no server contact)', async () => {
        const responseError = new DOMException('Invalid URL')
        axios.mockRejectedValue(responseError)

        const url = defaultAxiosConfig['url'].split(':').slice(0, -1).join(':')
        const expectedError = {
            status: -1,
            name: 'Server  (' + url + ') is offline.',
            message:
                'Server  (' +
                url +
                ') is offline or the URL is invalid. Please try again later',
        }

        const apiBaseClass = new ApiBaseClass()

        let error
        try {
            await apiBaseClass.get()
        } catch (errorResponse) {
            error = errorResponse
        }

        expect(error).toStrictEqual(expectedError)
    })

    test('failure - other errors', async () => {
        const responseError = new DOMException('Stack message')
        axios.mockRejectedValue(responseError)

        const expectedError = {
            //AxiosError
            status: -1,
            name: 'ConnectionError',
            message: 'Stack message',
        }

        const apiBaseClass = new ApiBaseClass()

        let error
        try {
            await apiBaseClass.get()
        } catch (errorResponse) {
            error = errorResponse
        }

        expect(error).toStrictEqual(expectedError)
    })

    test('failure - timeout client side.', async () => {
        const responseError = {
            request: {},
            message: 'timeout of 1ms exceeded',
        }
        axios.mockRejectedValue(responseError)

        const expectedError = {
            status: 0,
            name: 'No response received from server',
            message: 'timeout of 1ms exceeded',
        }

        const apiBaseClass = new ApiBaseClass()

        let error
        try {
            await apiBaseClass.get()
        } catch (errorResponse) {
            error = errorResponse
        }

        expect(error).toStrictEqual(expectedError)
    })

    test('failure - error from server.', async () => {
        const responseError = {
            response: {
                data: 'response from server',
                status: 400,
                statusText: 'Bad Request',
            },
        }
        axios.mockRejectedValue(responseError)

        const expectedError = {
            status: 400,
            name: 'Bad Request (400)',
            message: 'response from server',
        }

        const apiBaseClass = new ApiBaseClass()
        let error
        try {
            await apiBaseClass.get()
        } catch (errorResponse) {
            error = errorResponse
        }

        expect(error).toStrictEqual(expectedError)
    })
})
