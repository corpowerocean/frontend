import { ref, computed } from 'vue'

import { isSameSign } from '@shared/helpers/isSameSign'

// All returned values are given in the requested unit if not explicitly mentioned.
export function useDataItem(arg) {
    const requiredArguments = [
        'signalName',
    ]

    for (const requiredArgument of requiredArguments) {
        if (arg[requiredArgument] === undefined) {
            throw new Error('Missing required argument: ' + requiredArgument)
        }
    }

    // Non reactive argument
    const signalName = arg['signalName']
    const description = arg['description']
    const dataItemType = arg['dataItemType'] //signal, parameter, cmd
    const valueType = arg['valueType'] //enum, int, double, boolean
    const enumDefinition = arg["enumDef"] // {1:"ASD", 2:"BAS"}
    const _conversionExpression = arg['_conversionExpression']
    const _defaultValueOriginalUnit = arg['defaultValue']
    const _unitOriginal = arg['unit']
    const _unitHmi = arg['unitHmi'] //datetime, time or expression
    const _minOriginalUnit = arg['min']
    const _maxOriginalUnit = arg['max']


    // Functions
    const convertToHmiUnit = arg["convertToHmiUnit"] || ((value) => value);
    const convertFromHmiUnit = arg["convertFromHmiUnit"] || ((value) => value);
    const prettifyValue = arg["prettifyValue"] || ((value) => typeof value !== 'undefined' ? String(value) : undefined); // reduce number of digits (rounding) and use exponents, show enum values instead of strings


    // states
    const currentValueRaw = ref(undefined)


    const unit = computed(() => {
        return currentValue.value ===  currentValueRaw.value ? _unitOriginal : _unitHmi // needs unit test in convertToHmiUnit to verify the === works
    })


    const defaultValue = computed(() => {
        return convertToHmiUnit(_defaultValueOriginalUnit)
    })
    const currentValue = computed(() => {
        return convertToHmiUnit(currentValueRaw.value)
    })
    const currentValueStr = computed(() => {
        return prettifyValue(currentValue.value)
    })

    // If unit conversion flips the sign then max and min needs to change place as well.
    const min = computed(() => {
        const _min = convertToHmiUnit(_minOriginalUnit)
        if (!isSameSign(_min, _minOriginalUnit)) {
            return convertToHmiUnit(_maxOriginalUnit, _conversionExpression)
        }
        return _min
    })
    const minStr = computed(() => {
        return prettifyValue(min.value)
    })
    const max = computed(() => {
        const _max = convertToHmiUnit(_maxOriginalUnit)
        if (!isSameSign(_max, _maxOriginalUnit)) {
            return convertToHmiUnit(_minOriginalUnit, _conversionExpression)
        }
        return _max
    })
    const maxStr = computed(() => {
        return prettifyValue(max.value)
    })


    return {
        signalName,
        defaultValue,
        description,
        dataItemType,
        valueType,
        enumDefinition,

        currentValueRaw,
        currentValue,
        unit,
        currentValueStr,
        min,
        minStr,
        max,
        maxStr,

        convertToHmiUnit,
        convertFromHmiUnit,
        prettifyValue
    }
}


/* 
    // Left to do when we start implementing writable features
    modifyingValue, pendingValue, toDHSWUnit
    show warning
    this._forced_state = "free";
    this._allowedForcedState = ["forced", "pendingToFree", "free"];

    pending_value_as_dhsw_unit // still needed? not sure

    Define if a variable is writable
    if (key == "name") {
        Object.defineProperty(this, "writable", {
            value: isSignalWritable(portType),
            writable: false,
            configurable: false,
        });
    } */