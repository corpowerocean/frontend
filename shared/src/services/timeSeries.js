import apiBaseClass from '@shared/services/apiBaseClass'

export class TimeSeries extends apiBaseClass {
    constructor() {
        super()
        this.ip = 'http://10.39.100.10'
        this.port = ':3003'
        this.endpoint = '/time-series'
    }
}
