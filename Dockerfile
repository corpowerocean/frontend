FROM ubuntu:22.04 as base


# Install required dependencies
RUN apt-get update && \
    apt-get install -y \
    curl \
    wget \
    git \
    && rm -rf /var/lib/apt/lists/*

RUN apt-get install curl ca-certificates -y
RUN curl -fsSL https://deb.nodesource.com/setup_22.x | bash -
RUN apt-get install -y nodejs

# Install PNPM globally
RUN npm install -g pnpm

EXPOSE 5173

