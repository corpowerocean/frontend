import axios from 'axios'

export class ApiBaseClass {
    constructor() {
        this.ip = 'http://127.0.0.1'
        this.port = ':3001'
        this.endpoint = '/'
        this.url = this.ip + this.port + this.endpoint
    }

    async get(data = {}) {
        return await this._request('get', data)
    }

    async post(data = {}) {
        return await this._request('post', data)
    }

    async _request(method, data = {}) {
        try {
            const result = await axios({
                method: method,
                url: this.url,
                timeout: 30000,
                data: data,
            })
            return result
        } catch (error) {
            throw this._handleError(error)
        }
    }
    _handleError(error) {
        if (error.response) {
            // Request made and the server responded with a status code outside 2xx
            return {
                status: error.response.status,
                name:
                    error.response.statusText +
                    ' (' +
                    error.response.status +
                    ')',
                message: error.response.data,
            }
        } else if (error.request) {
            // Request made but no response is received from the server. (timeout, network error, cors etc)
            return {
                status: 0,
                message: error.message,
                name: 'No response received from server',
            }
        } else if (
            error instanceof DOMException &&
            error.message.includes('Invalid URL')
        ) {
            // This error occurs due to an invalid URL or server offline.
            return {
                status: -1,
                name: 'Server  (' + this.ip + ') is offline.',
                message:
                    'Server  (' +
                    this.ip +
                    ') is offline or the URL is invalid. Please try again later',
            }
        } else {
            // Something happened in setting up the request that triggered an Error (wrong ip etc)
            return {
                status: -1,
                name: 'ConnectionError',
                message: error.message,
            }
        }
    }
}
