# Testing

## About

### The testing stack

These tools are used for testing

-   Vitest: The test framework, with assertions, mocking etc
-   Vue Test Utils: Is used to render components and query the DOM
-   Playwright: Playwright is a cross-browser testing framework that allows you to automate browser actions and perform various tests, such as UI testing, end-to-end testing, and more.

### Types of tests

Three types of tests exist in this project

-   Unit test (vitest): Whitebox tests, where we mock as many dependencies as possible. They can cover single functions, classes, composables, or modules but should not depend on DOM
-   Component tests (Vitest and Vue Test Utils): Component tests should catch issues relating to your component's props, events, slots that it provides, styles, classes, lifecycle hooks, and more.
-   End-to-end test (playwright): Testing your application by navigating through entire pages in a real browser.
    -   TBD: Either run isolated to the frontend with mocked backend or in a staged environment with all backend services running. or both.

## Test folder structure

-   The test files for unit and component test should be named as the file that they are testing but with \*.test.js after the name.
-   They should be place in a separate test folder this will allow for a clear seperation of concern and make the src folder easier to navigate and to understand. vs code extension vitest is recommended for an overview of existing test.
-   Mocks and other test utils should also be placed in the /tests folder.

## API references used when writing tests

How to render the component
https://testing-library.com/docs/vue-testing-library/api

How to query DOM elements and interact with them
https://testing-library.com/docs/queries/about

How to mock modules etc
https://vitest.dev/guide/mocking.html#example-2

For assertions
https://vitest.dev/api/

## Things to consider

Vitest has support for snapshots but for now this is not used (and the **snapshots** folder are ignored in git). Should we use it and if so how?
