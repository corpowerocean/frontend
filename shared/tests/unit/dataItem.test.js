import { expect, test, describe} from 'vitest'
import { useDataItem } from '@shared/composable/dataItem'
import { ref } from 'vue'

describe('dataItem', () => {
    test('missing required arguments, doHmiUnitConversion', () => {
        const inputArg = {}

        expect(() => useDataItem(inputArg)).toThrowError(
            'Missing required argument: signalName'
        )
    })

    test('default values/undefined', () => {
        const inputArg = {
            signalName: 'name1',
        }

        const dataItem = useDataItem(inputArg)

        const nonReactiveData = {
            signalName: 'name1',
            description: undefined,
            dataItemType: undefined,
            valueType: undefined,
            enumDefinition: undefined
        }
        
        const reactiveData = {
            defaultValue: undefined,
            min: undefined,
            minStr: undefined,
            max: undefined,
            maxStr: undefined,
            unit: undefined,
            currentValueRaw: undefined,
            currentValue: undefined,
            currentValueStr: undefined,
        }


        for (const key in nonReactiveData) {
            expect(dataItem[key]).toStrictEqual(nonReactiveData[key])
        }
        for (const key in reactiveData) {
            expect(dataItem[key].value).toBe(reactiveData[key])
        }
        //assert methods
        expect(dataItem.convertToHmiUnit(1)).toBe(1)
        expect(dataItem.convertFromHmiUnit(1)).toBe(1)
        expect(dataItem.prettifyValue(1)).toBe("1")
    })

    test('hmi and original unit - no conversion', () => {
        const inputArg = {
            signalName: 'name1',
            unit: 'K',
            unitHmi: 'degC',
        }

        const dataItem = useDataItem(inputArg)

        expect(dataItem['unit'].value).toStrictEqual('K')
    })

    test('hmi and original unit - do convertsion', () => {
        const inputArg = {
            signalName: 'name1',
            unit: 'K',
            unitHmi: 'degC',
            convertToHmiUnit: ((value) => value * 1)
        }

        const dataItem = useDataItem(inputArg)

        expect(dataItem['unit'].value).toStrictEqual('degC')
    })


    test('flip min and max', () => {
        const inputArg = {
            signalName: 'name1',
            min: 1,
            max: 10,
            unit: 'K',
            unitHmi: 'degC',
            convertToHmiUnit: ((value) => value * -1)
        }

        const dataItem = useDataItem(inputArg)
        dataItem.currentValueRaw.value = 5

        expect(dataItem['unit'].value).toStrictEqual('degC')
        expect(dataItem['currentValue'].value).toStrictEqual(-5)
        expect(dataItem['currentValueStr'].value).toStrictEqual("-5")
        expect(dataItem['min'].value).toStrictEqual(-10)
        expect(dataItem['minStr'].value).toStrictEqual("-10")
        expect(dataItem['max'].value).toStrictEqual(-1)
        expect(dataItem['maxStr'].value).toStrictEqual("-1")
    })

    test('change conversion', () => {
        const doConversion = ref(false)
        const inputArg = {
            signalName: 'name1',
            defaultValue: 2,
            min: 1,
            max: 10,
            unit: 'K',
            unitHmi: 'degC',
            convertToHmiUnit: ((value) => {
                if(doConversion.value) {
                    return value *2
                } else {
                    return value
                }
            }),
            prettifyValue: ((value) => value + "!")
        }

        const dataItem = useDataItem(inputArg)
        dataItem.currentValueRaw.value = 5

        expect(dataItem['unit'].value).toStrictEqual('K')
        expect(dataItem['defaultValue'].value).toStrictEqual(2)
        expect(dataItem['currentValue'].value).toStrictEqual(5)
        expect(dataItem['currentValueStr'].value).toStrictEqual("5!")
        expect(dataItem['min'].value).toStrictEqual(1)
        expect(dataItem['minStr'].value).toStrictEqual("1!")
        expect(dataItem['max'].value).toStrictEqual(10)
        expect(dataItem['maxStr'].value).toStrictEqual("10!")

        doConversion.value = true

        expect(dataItem['unit'].value).toStrictEqual('degC')
        expect(dataItem['defaultValue'].value).toStrictEqual(4)
        expect(dataItem['currentValue'].value).toStrictEqual(10)
        expect(dataItem['currentValueStr'].value).toStrictEqual("10!")
        expect(dataItem['min'].value).toStrictEqual(2)
        expect(dataItem['minStr'].value).toStrictEqual("2!")
        expect(dataItem['max'].value).toStrictEqual(20)
        expect(dataItem['maxStr'].value).toStrictEqual("20!")


    })
})



